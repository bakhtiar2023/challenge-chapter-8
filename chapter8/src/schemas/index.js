import * as yup from "yup"

const passwordRules = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{5,}$/

const loginSchema = yup.object().shape({
    username : yup.string().required("username should not empty").min(3,"username too short, min 3 character").max(10,"username too long, max 10 character"),
    password : yup.string().required("password should not empty").min(6,"password too short, min 6 character")
})

const registrationSchema = yup.object().shape({
    username : yup.string().required("username should not empty").min(3,"username too short, min 3 character").max(10,"username too long, max 10 character"),
    email : yup.string().email("Invalid format email").required("email should not empty"),
    password: yup.string().matches(passwordRules,"Please create stronger password").min(6,"password too short, min 6 character").required("password should not empty")
})

const createRoomSchema = yup.object().shape({
    roomName : yup.string().required("room name should not empty").min(3,"room name too short, min 3 character").max(10,"room name too long, max 10 character")
})

export {loginSchema,registrationSchema,createRoomSchema}