import React, { useState} from "react";
import "./index.css";
import rock from "../assets/images/batu.png";
import paper from "../assets/images/kertas.png";
import scissors from "../assets/images/gunting.png";
import { Link, useLocation } from "react-router-dom";
import { Title } from "../components/elements";
const PlayerVsPlayer = () => {
  const {state} = useLocation()
  const [result, setResult] = useState(state.value.winner);
  const [player2Choice, setPlayer2Choice] = useState(state.value.player2Choice);
  const [player1Choice, setPlayer1Choice] = useState(state.value.player1Choice);

  const setRock = () => {
    setPlayer1Choice("rock");
  };
  const setPaper = () => {
    setPlayer1Choice("paper");
  };
  const setScissors = () => {
    setPlayer1Choice("scissors");
  };
  console.log(state)
  return (
    <div className="bigContainer">
    <div className="playerVsComContainer left-container">
      <div className="playerChoiceContainer">
        <Title classProps="playerTitle title fs-3 mb-4">{state.value.player1Name}</Title>
        <Link
          className={player1Choice === "rock" ? "choiced" : "player1 choices"}
          style={
            player1Choice !== ""
              ? { pointerEvents: "none" }
              : { pointerEvents: "auto" }
          }
          onClick={setRock}
        >
          <img src={rock} alt="rock" className="rock" />
        </Link>
        <Link
          className={player1Choice === "paper" ? "choiced" : "player1 choices"}
          style={
            player1Choice !== ""
              ? { pointerEvents: "none" }
              : { pointerEvents: "auto" }
          }
          onClick={setPaper}
        >
          <img src={paper} alt="paper" className="paper" />
        </Link>
        <Link
          className={player1Choice === "scissors" ? "choiced" : "player1 choices"}
          style={
            player1Choice !== ""
              ? { pointerEvents: "none" }
              : { pointerEvents: "auto" }
          }
          onClick={setScissors}
        >
          <img src={scissors} alt="scissors" className="scissors" />
        </Link>
      </div>
      <div className="resultContainer">
        <div className="resultGame mb-5">
          <Title classProps="result title">{result === null ? "Chose your choice" : `${result} win!`}</Title>
        </div>
      </div>
      <div className="playerChoiceContainer">
        <Title classProps="playerTitle title fs-3 text-transform-uppercase text-center mb-0">{result === null ? "Waiting for player 2 ...":`${state.value.player2Name}`}</Title>
        <div className={player2Choice === "rock" ? "choiced" : "comChoices"}>
          <img src={rock} alt="rock" className="rock" />
        </div>
        <div className={player2Choice === "paper" ? "choiced" : "comChoices"}>
          <img src={paper} alt="paper" className="paper" />
        </div>
        <div className={player2Choice === "scissors" ? "choiced" : "comChoices"}>
          <img src={scissors} alt="scissors" className="scissors" />
        </div>
      </div>
    </div>
  </div>
  )
}

export default PlayerVsPlayer