import React from "react";
import "./index.css";
import LeftContainer from "../components/layouts/LeftContainer";
import RightContainer from "../components/layouts/RightContainer";

const Games = () => {
  return (
    <div className="bigContainer">
      <LeftContainer />
      <RightContainer />
    </div>
  );
};

export default Games;
