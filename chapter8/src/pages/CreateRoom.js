import React,{useState,useEffect} from "react";
import "./index.css";
import {useFormik} from "formik"
import {createRoomSchema} from "../schemas/index"
import { Title } from "../components/elements";
import { Link, useNavigate } from "react-router-dom";
import rock from "../assets/images/batu.png";
import paper from "../assets/images/kertas.png";
import scissors from "../assets/images/gunting.png";

const CreateRoom = () => {
    const navigate = useNavigate()
    const [isErrors, setIsErrors] = useState(true)
    const {values,errors,handleChange,handleSubmit} = useFormik({
      initialValues:{
        roomName: "",
      },
      validationSchema: createRoomSchema
    })
    const handleParse = (event) => {
      setRoomData({...roomData,roomName:event.target.value})
    }
    const handleClick = () => {
      console.log(roomData)
      navigate("/games")
    }
    const [roomData, setRoomData] = useState({roomName : "",player1Choice : ""})
    useEffect(() => {
      Object.keys(errors).length === 0 ? setIsErrors(false):setIsErrors(true)
    }, [errors])
    
  return (
    <div className="bigContainer">
      <div className="createRoom playerVsComContainer left-container">
        <form className="inputCreateRoom playerChoiceContainer row justify-content-center align-items-center" onSubmit={handleSubmit}>
          <label htmlFor="roomName" className="text-center mb-3 fw-bold text-dark fs-3">CREATE ROOM PVP</label>
          <input className="inputRoom input" id="roomName" placeholder="input room name here!" onChange={handleParse&&handleChange} value={values.roomName} name="roomName"/>
          <p className="errorsMessage">{errors.roomName}</p>
          <button className="button text-center mt-3 text-transform-uppercase " onClick={handleClick} disabled={isErrors === true ? true : false}>create room</button>
        </form>
        <hr className="underlineRoom"/>
        <div className="choiceCreateRoom playerChoiceContainer">
          <Title classProps="playerChoice title text-center">
            Choose Your Choice:
          </Title>
          <div className="choiceContainer p-0">
            <Link className={roomData.player1Choice === "rock" ? "roomChoice choiced":"player player1 choices"} style={roomData.player1Choice === "" ? {pointerEvents:"auto"} : {pointerEvents:"none"}} onClick={()=>{setRoomData({...roomData,player1Choice:"rock"})}}>
              <img src={rock} alt="rock" className="rockRoom" onClick={()=>{setRoomData({...roomData,player1Choice:"rock"})}}/>
            </Link>
            <Link className={roomData.player1Choice === "paper" ? "roomChoice choiced":"player player1 choices"} style={roomData.player1Choice === "" ? {pointerEvents:"auto"} : {pointerEvents:"none"}} onClick={()=>{setRoomData({...roomData,player1Choice:"paper"})}}>
              <img src={paper} alt="paper" className="paperRoom" onClick={()=>{setRoomData({...roomData,player1Choice:"paper"})}}/>
            </Link>
            <Link className={roomData.player1Choice === "scissors" ? "roomChoice choiced":"player player1 choices"} style={roomData.player1Choice === "" ? {pointerEvents:"auto"} : {pointerEvents:"none"}} onClick={()=>{setRoomData({...roomData,player1Choice:"scissors"})}}>
              <img src={scissors} alt="scissors" className="scissorsRoom" onClick={()=>{setRoomData({...roomData,player1Choice:"scissors"})}}/>
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
};

export default CreateRoom;
