import React, { useState } from 'react'
import "./index.css"
import FormLogin from "../components/forms/FormLogin"
import FormRegistration from "../components/forms/FormRegistration"
import Overlay from "../components/layouts/Overlay"
import { useLocation } from 'react-router-dom'

const SignupSignin = (props) => {
    let {state} = useLocation()
    const [value, setvalue] = useState(state.value)
    const loginClick = () => {
        setvalue(true)
    }
    const registrationClick = () => {
        setvalue(false)
    }
  return (
    <div className="bigContainer">
        <div className='container'>
            <FormLogin value={value} loginClick={loginClick}/>
            <FormRegistration value={value} registrationClick={registrationClick}/>
            <Overlay value={value} registrationClick={registrationClick} loginClick={loginClick} />
        </div>
    </div>
  )
}

export default SignupSignin