import React,{useState,useEffect} from "react";
import {registrationSchema} from "../../schemas/index"
import { useFormik } from "formik";
import { Title} from "../elements/index";
import "./index.css";

const FormRegistration = (props) => {
  const { value } = props;
  const [isErrors, setIsErrors] = useState(true)
  
  const {values,errors,handleSubmit,handleChange} = useFormik({
    initialValues:{
      username: "",
      email:"",
      password:""
    },
    validationSchema:registrationSchema
  })
  const [user, setUser] = useState({username:values.username,email:values.email,password:values.password})
  useEffect(() => {
    Object.keys(errors).length === 0 ? setIsErrors(false):setIsErrors(true)
  }, [errors])
  

  return (
    <div
      className={
        value !== true ? "registrationOnClick registration" : "registration"
      }
    >
      <form className="form" id="form2" onSubmit={handleSubmit}>
        <Title classProps="title">Sign Up</Title>
        <input type="text" placeholder="Username" className="input" name="username" value={values.username} onChange={handleChange} />
        <p className="errorsMessage">{errors.username}</p>
        <input type="email" placeholder="Email" className="input" name="email" onChange={handleChange} value={values.email}/>
        <p className="errorsMessage">{errors.email}</p>
        <input type="password" placeholder="Password" className="input" name="password" onChange={handleChange} value={values.password}/>
        <p className="errorsMessage">{errors.password}</p>
        <button className="button" handleClick={() => {console.log(user)}} disabled={isErrors === true ? true : false}>
          Sign Up
        </button>
      </form>
    </div>
  );
};

export default FormRegistration;
