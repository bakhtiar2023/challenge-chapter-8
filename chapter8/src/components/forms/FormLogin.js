import React, { useState,useEffect} from "react";
import { useNavigate } from "react-router-dom";
import { Title, Anchor } from "../elements/index";
import {useFormik} from "formik"
import {loginSchema} from "../../schemas/index"
import "./index.css";

const FormLogin = (props) => {
  const navigate = useNavigate()
  const { value } = props;
  const [isErrors, setIsErrors] = useState(true)
  const [user, setUser] = useState({ username: "", password: "" });

  const {values, errors,handleChange,handleSubmit,touched} = useFormik({
    initialValues:{
      username : "",
      password : ""
    },
    validationSchema: loginSchema
  })

  useEffect(() => {
    Object.keys(errors).length === 0 ? setIsErrors(false) : setIsErrors(true)
  }, [errors])
  
  const handleLogin = () => {
    setUser({...user,username:values.username,password:values.password})
    const accessToken = values.username
    localStorage.setItem("accessToken", accessToken)
    navigate("/games")
  }
  return (
    <div className={value !== true ? "loginOnClick login" : "login"}>
      <form
        className="form"
        id="form1"
        onSubmit={handleSubmit}
      >
        <Title classProps="title">Sign In</Title>
        <input
          type="text"
          placeholder="Username or Email"
          id="loginUsername"
          className="input"
          name="username"
          onChange={handleChange}
          value={values.username}
        />
        <p className={errors.username && touched.username ? "":"errorsMessage"}>{errors.username}</p>
        <input
          type="password"
          placeholder="Password"
          id="loginPassword"
          name="password"
          className="input"
          value={values.password}
          onChange={handleChange}
        />
        <p className={errors.password && touched.password ? "":"errorsMessage"}>{errors.password}</p>
        <Anchor classProps="anchor">
          Forgot username/password?
        </Anchor>
        <button
          className="button"
          onClick={handleLogin}
          disabled = {isErrors === true ? true : false}
        >
          Sign In
        </button>
      </form>
    </div>
  );
};

export default FormLogin;
