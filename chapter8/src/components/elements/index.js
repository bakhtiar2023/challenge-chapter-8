import Anchor from "./atoms/Anchor";
import Button from "./atoms/Button";
import Input from "./atoms/Input";
import Paragraph from "./atoms/Paragraph";
import Title from "./atoms/Title";

export {Anchor,Button,Input,Paragraph,Title}