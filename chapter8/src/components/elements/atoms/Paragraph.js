import '../index.css'

const Paragraph = (props) => {
    const {classProps,children} = props
    return (
        <p className={classProps}>{children}</p>
    )
}

export default Paragraph