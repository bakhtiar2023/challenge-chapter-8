import '../index.css'

const Title = (props) => {
    const {classProps,children} = props
    return (
        <h1 className={classProps}>{children}</h1>
    )
}

export default Title