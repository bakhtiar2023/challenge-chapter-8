import '../index.css'

const Input = (props) => {
    const {id,typeProps,placeholder,classProps, onChange} = props
    return (
        <input type={typeProps} placeholder={placeholder} className={classProps} id={id} onChange={(e)=>{onChange(e)}}/>
    )
}

export default Input