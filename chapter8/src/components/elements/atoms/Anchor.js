import "../index.css";

const Anchor = (props) => {
  const {href,classProps,children,handleClick} = props
  return (
    <a href={href} className={classProps} onClick={handleClick}>
      {children}
    </a>
  );
};

export default Anchor;
