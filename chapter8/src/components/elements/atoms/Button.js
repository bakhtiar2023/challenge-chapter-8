import '../index.css'

const Button = (props) => {
    const {classProps,handleClick,children,onSubmit} =props
    return (
        <button className={classProps} onClick={handleClick} onSubmit={onSubmit}>{children}</button>
    )
}

export default Button