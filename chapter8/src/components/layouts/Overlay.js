import React from 'react'
import LeftPanel from '../overlayPanels/LeftPanel'
import RightPanel from '../overlayPanels/RightPanel'
import "./index.css"

const Overlay = (props) => {
    const {value,loginClick,registrationClick} = props
  return (
    <div className={value !== true ? "ovelayContainerOnClik overlayContainer" : "overlayContainer"}>
        <div className={value !== true ? "overlayOnclick overlay" : "overlay"}>
            <LeftPanel value={value} loginClick={loginClick} />
            <RightPanel value={value} registrationClick={registrationClick}/>
        </div>
    </div>
  )
}

export default Overlay