import React, { useState } from "react";
import { Link} from "react-router-dom";
import { Title } from "../elements/index";
import "./index.css";

const LeftContainer = () => {
  const [roomsData, setRoomsData] = useState([
    {
      id: 1,
      title: "Game One",
      player1Name: "Naruto",
      player1Choice: "rock",
      player2Name: "Sanji",
      player2Choice: "scissors",
      winner: "Naruto",
    },
    {
      id: 2,
      title: "Game Two",
      player1Name: "Gundala",
      winner: null,
    },
    {
      id: 3,
      title: "Game Three",
      player1Name: "Giant",
      player1Choice: "paper",
      player2Name: "Suneo",
      player2Choice: "scissors",
      winner: "Suneo",
    },
    {
      id: 4,
      title: "Game Four",
      player1Name: "Steve Roger",
      player1Choice: "scissors",
      player2Name: "Adipati",
      player2Choice: "rock",
      winner: "Adipati",
    },
    {
      id: 5,
      title: "Game Five",
      player1Name: "Gundala",
      winner: null,
    },
    {
      id: 6,
      title: "Game Six",
      player1Name: "Gundala",
      player1Choice: "paper",
      player2Name: "Saras",
      player2Choice: "rock",
      winner: "Gundala",
    },
    {
      id: 7,
      title: "Game Seven",
      player1Name: "Guts",
      winner: null,
    },
    {
      id: 8,
      title: "Game Eight",
      player1Name: "Guts",
      player1Choice: "paper",
      player2Name: "Griffith",
      player2Choice: "scissors",
      winner: "Griffith",
    },
  ]);
  return (
    <div className="left-container">
      <div className="pvcGame">
        <Link className="playVCom" to={"/player-vs-com"}>
          <Link className="btnVCom" to={"/player-vs-com"}>Player Vs Com</Link>
        </Link>
        <Link className="playVCom" to={"/create-room"}>
          <Link className="btnVCom" to={"/create-room"}>Create Room PVP</Link>
        </Link>
      </div>
      <Title classProps="roomTitle title ms-4 me-4 lh-2">Rooms : </Title>
      <div className="roomContainer">
        <div className="row ms-3">{
          roomsData.map((roomData) => {
            return (
              <div className="dataCourier" key={roomData.id}>
                <Link className="courierText" to={"/player-vs-player"} state={{value : roomData}}>
                  <>Room: {roomData.title}</>
                  <br></br>
                  <>
                    Winner: {roomData.winner !== null ? roomData.winner : "....."}
                  </>
                </Link>
              </div>
            );
          })
        }</div>
      </div>
    </div>
  );
};

export default LeftContainer;
