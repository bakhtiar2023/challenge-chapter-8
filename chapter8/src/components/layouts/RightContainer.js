import React, {useState}from 'react'
import { Link } from 'react-router-dom';
import { Title} from "../elements/index";
import profile from "../../assets/icons/profile-icon.svg";
import "./index.css"

const RightContainer = () => {
  const accessToken = localStorage.getItem("accessToken")
  const [value, setValue] = useState(false)
  const [biodata, setBiodata] = useState({
    fullname: "",
    address: "",
    phoneNumber: "",
    dateOfBirth: "",
  });
  const editClick = () => {
    setValue(true)
  }
  const updateClick = () => {
    setValue(false)
  }
  return (
    <div className="right-container px-4">
        <div className="upper-right-container mt-1 mx-4">
          <div className="top-upper-right-container mt-2 pe-2">
            <Link
              className={
                value !== true
                  ? "gameHistoryText text-dark text-center text-decoration-underline me-4"
                  : "gameHistoryText text-dark text-center text-decoration-underline me-5 ms-0"
              }
              to={"/game-history"}
            >
              Game History
            </Link>
            <i
              class={value !== true ? "editIcons fa fa-pencil" : null}
              aria-hidden="true"
              onClick={editClick}
            />
          </div>
          <img src={profile} alt="profile" className="rounded-circle mt-1" />
          <Title classProps="profileTitle title">{accessToken}</Title>
        </div>
        <div
      className={
        value !== true
          ? "updatedContainer lower-right-container"
          : "lower-right-container row mt-2 pb-5 pt-2 ps-4"
      }
    >
      <div className="biodata">Fullname: {biodata.fullname === "" ? "....." : biodata.fullname}</div>
      <div className="biodata">Address: {biodata.address === "" ? "....." : biodata.address}</div>
      <div className="biodata">Phone: {biodata.phoneNumber === "" ? "....." : biodata.phoneNumber}</div>
      <div className="biodata">Date of Birth: {biodata.dateOfBirth === "" ? "....." : biodata.dateOfBirth}</div>
    </div>
    <div
        className={
          value !== true
            ? "notUpdated lower-right-container-input"
            : "lower-right-container-input formBiodata mt-2 ps-5"
        }
      >
        <div className="commandInput text-center text-capitalize fw-regular fs-6 text-dark mb-1 me-5">
          Please input your biodata
        </div>
        <label htmlFor="fullnameInput" className="laberInput fs-6 mt-0">
          Fullname:
        </label>
        <input
          type="text"
          placeholder={
            biodata.fullname === "" ? "Fullname" : `${biodata.fullname}`
          }
          className="inputBiodata inputData mt-0"
          id="fullnameInput"
          disabled={false}
          onChange={(e) => {
            setBiodata({ ...biodata, fullname: e.target.value });
          }}
        />
        <label htmlFor="addressInput" className="laberInput fs-6 mt-0">
          Address:
        </label>
        <input
          type="text"
          placeholder={
            biodata.address === "" ? "Address" : `${biodata.address}`
          }
          className="inputBiodata inputData mt-0"
          id="addressInput"
          disabled={false}
          onChange={(e) => {
            setBiodata({ ...biodata, address: e.target.value });
          }}
        />
        <label htmlFor="phoneNumber" className="laberInput fs-6 mt-0">
          Phone Number:
        </label>
        <input
          type="text"
          placeholder={
            biodata.phoneNumber === ""
              ? "Phone Number"
              : `${biodata.phoneNumber}`
          }
          className="inputBiodata inputData mt-0"
          id="phoneNumber"
          disabled={false}
          onChange={(e) => {
            setBiodata({ ...biodata, phoneNumber: e.target.value });
          }}
        />
        <label htmlFor="dateOfBrith" className="laberInput fs-6 mt-0">
          Date of Birth:
        </label>
        <input
          type="text"
          placeholder={
            biodata.dateOfBirth === ""
              ? "Date of Birth"
              : `${biodata.dateOfBirth}`
          }
          className="inputBiodata inputData mt-0"
          id="dateOfBrith"
          disabled={false}
          onChange={(e) => {
            setBiodata({ ...biodata, dateOfBirth: e.target.value });
          }}
        />
        <button
          className="updateBiodata button ms-5 py-3"
          onClick={updateClick}
        >
          update Biodata
        </button>
      </div>
      </div>
  )
}

export default RightContainer