import { Navigate } from "react-router-dom"


const RequiredAuth = ({children}) => {
    const accessToken = localStorage.getItem("accessToken")
    if(!accessToken){
        return <Navigate to={"/login-or-register"} state={{value:true}}/>
    }
  return (<>{children}</>)
}

export default RequiredAuth