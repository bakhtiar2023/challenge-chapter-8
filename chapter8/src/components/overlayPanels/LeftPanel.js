import React from "react";
import { Title, Paragraph, Button } from "../elements/index";
import "./index.css";

const LeftPanel = (props) => {
  const { value , loginClick} = props;
  return (
    <div
      className={
        value !== true
          ? "leftOvelayPanelOnClick overlayPanel"
          : "leftOvelayPanel overlayPanel"
      }
    >
      <Title classProps="titleOverlay title">Welcome Back!</Title>
      <Paragraph classProps="paragraph">
        To keep connected with us please login with your personal info
      </Paragraph>
      <Button classProps="ghostButton button" handleClick={loginClick}>Sign In</Button>
    </div>
  );
};

export default LeftPanel;
