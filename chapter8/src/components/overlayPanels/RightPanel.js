import React from "react";
import { Title, Paragraph ,Button} from "../elements/index";
import "./index.css";

const RightPanel = (props) => {
  const { value , registrationClick} = props;
  return (
    <div
      className={
        value !== true
          ? "rightOverlayPanelOnClick overlayPanel"
          : "rightOverlayPanel overlayPanel"
      }
    >
      <Title classPorps="titleOverlay title">Hello Friend!</Title>
      <Paragraph classProps="paragraph">
        Enter your personal details and start journey with us
      </Paragraph>
      <Button classProps="ghostButton button" handleClick={registrationClick}>Sign Up</Button>
    </div>
  );
};

export default RightPanel;
