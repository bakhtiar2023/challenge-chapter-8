import { Routes, Route } from "react-router-dom";
import MainPage from "./pages/MainPage";
import LoginOrRegister from "./pages/LoginOrRegister";
import Games from "./pages/Games";
import PlayerVsCom from "./pages/PlayerVsCom";
import CreateRoom from "./pages/CreateRoom";
import GameHistory from "./pages/GameHistory";
import PlayerVsPlayer from "./pages/PlayerVsPlayer"
import NotFound from "./pages/404/404";
import RequiredAuth from "./components/RequiredAuth";

function App() {
  return (
    <Routes>
      <Route path="/" element={<MainPage />}>
        <Route path="/games" element={<RequiredAuth><Games /></RequiredAuth>} />
        <Route path="/player-vs-com" element={<RequiredAuth><PlayerVsCom /></RequiredAuth>} />
        <Route path="/create-room" element={<RequiredAuth><CreateRoom /></RequiredAuth>} />
        <Route path="/game-history" element={<RequiredAuth><GameHistory/></RequiredAuth>}/>
        <Route path="/player-vs-player" element ={<RequiredAuth><PlayerVsPlayer/></RequiredAuth>}/>
        <Route path="/login-or-register" element={<LoginOrRegister />} />
        <Route path="*" element={<NotFound/>}/>
      </Route>
    </Routes>
  );
}

export default App;
